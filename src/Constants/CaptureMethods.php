<?php

namespace MountPay\Constants;

final class CaptureMethods
{
    public const MANUAL = 'Manual';         // payment is handled on "capture" action
    public const AUTOMATIC = 'Automatic';   // payment is handled automatically on "execute" action

    private function __construct() { }
}
