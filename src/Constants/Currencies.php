<?php

namespace MountPay\Constants;

final class Currencies
{
    public const RON = 'RON';
    public const EUR = 'EUR';
    public const USD = 'USD';

    private function __construct() { }
}
