<?php

namespace MountPay\Constants;

final class IntentTypes
{
    public const PAYMENT = 'Payment';
    public const CARD_REGISTRATION = 'CardRegistration';

    private function __construct() { }
}
