<?php

namespace MountPay\Constants;

final class PaymentIntentEvents
{
    public const FAILED = 'payment_intent.payment_failed';
    public const SUCCEEDED = 'payment_intent.succeeded';

    private function __construct() { }
}
