<?php

namespace MountPay\Constants;

final class PaymentMethods
{
    public const CARD = 'card';
    public const APPLE_PAY = 'apple-pay';
    public const GOOGLE_PAY = 'google-pay';
    public const REVOLUT_PAY = 'revolut-pay';
    public const CREDIT = 'credit';
    public const SMS = 'sms';

    private function __construct() { }
}
