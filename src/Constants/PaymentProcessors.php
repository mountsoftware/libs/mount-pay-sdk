<?php

namespace MountPay\Constants;

final class PaymentProcessors
{
    public const STRIPE = 'stripe';
    public const NETOPIA = 'netopia';
    public const AM_PARCAT_CREDIT = 'am-parcat-credit';
    public const REVOLUT = 'revolut';

    private function __construct() { }
}
