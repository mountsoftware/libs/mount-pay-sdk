<?php

namespace MountPay\Constants;

final class PaymentStatuses
{
    public const CREATED = 'created';
    public const PENDING = 'pending';
    public const SUCCESS = 'success';
    public const FAILED = 'failed';
    public const REFUNDED = 'refunded';

    private function __construct() { }
}
