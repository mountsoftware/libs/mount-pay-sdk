<?php

namespace MountPay\Models\Data;

use MountPay\Models\DataTransfer\Customer;

class PaymentIntent
{
    private string $id;
    private array $paymentProcessors;
    private Transaction $transaction;
    private Customer $customer;
    private string $type;
    private string $captureMethod;
    private string $clientMetadata;
    private string $paymentToken;
    private array $availableProcessors;
    private string $status;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getPaymentProcessors(): array
    {
        return $this->paymentProcessors;
    }

    /**
     * @return Transaction
     */
    public function getTransaction(): Transaction
    {
        return $this->transaction;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string
     */
    public function getCaptureMethod(): string
    {
        return $this->captureMethod;
    }

    /**
     * @return string
     */
    public function getClientMetadata(): string
    {
        return $this->clientMetadata;
    }

    /**
     * @return string
     */
    public function getPaymentToken(): string
    {
        return $this->paymentToken;
    }

    /**
     * @return array
     */
    public function getAvailableProcessors(): array
    {
        return $this->availableProcessors;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

}