<?php

namespace MountPay\Models\Data;

class PaymentIntentEvent
{
    private string $id;
    private string $event;
    private PaymentIntent $paymentIntent;

    public function __construct(array $props) {
        $this->id = $props['id'];
        $this->event = $props['event'];
        $this->paymentIntent = new PaymentIntent($props['paymentIntent']);
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEvent(): string
    {
        return $this->event;
    }

    /**
     * @return PaymentIntent
     */
    public function getPaymentIntent(): PaymentIntent
    {
        return $this->paymentIntent;
    }

}