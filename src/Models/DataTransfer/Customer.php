<?php

namespace MountPay\Models\DataTransfer;

class Customer
{
    /**
     * id from paymentMiddleware
     */
    public ?string $id = null;

    /**
     * id from client
     */
    public string $clientId;

    public string $email;

}