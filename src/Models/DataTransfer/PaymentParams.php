<?php

namespace MountPay\Models\DataTransfer;

class PaymentParams
{
    public array $enabledProcessors = [];
    public array $disabledProcessors = [];
    public array $enabledMethods = [];
    public array $disabledMethods = [];

    public array $paymentCards = [];

}