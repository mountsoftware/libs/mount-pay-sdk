<?php

namespace MountPay\Models\DataTransfer;


use MountPay\Constants\CaptureMethods;
use MountPay\Constants\Currencies;
use MountPay\Constants\IntentTypes;

class TransactionData
{
    public Customer $customer;
    public float $amount;
    public string $currency = Currencies::RON;
    public bool $serverToServer = false;
    public PaymentParams $paymentParams;
    public UrlParams $urlConfig;
    public string $captureMethod = CaptureMethods::AUTOMATIC;
    public string $type = IntentTypes::PAYMENT;
    public array $metadata = [];

    public function __construct(Customer $user, float $amount)
    {
        $this->paymentParams = new PaymentParams();
        $this->urlConfig = new UrlParams();
        $this->customer = $user;
        $this->amount = $amount;
    }

}