<?php

namespace MountPay\Models\DataTransfer;

class UrlParams
{
    public ?string $successUrl = null;
    public ?string $failedUrl = null;
    public ?string $webhookUrl = null;

}