<?php

namespace MountPay\Services;

use GuzzleHttp\Exception\GuzzleException;
use MountPay\Models\Data\PaymentIntent;
use MountPay\Models\DataTransfer\TransactionData;
use MountPay\Utils\GuzzleClient;
use MountPay\Utils\SerializerTrait;

class PaymentMiddlewareService
{
    const CAPTURE_METHOD_AUTOMATIC = 'Automatic';
    const CAPTURE_METHOD_MANUAL = 'Manual';

    use SerializerTrait;

    protected GuzzleClient $guzzleClientImpl;
    private string $apiKey;
    private string $baseURL;

    public function __construct(string $apiKey, string $baseURL)
    {
        $this->apiKey = $apiKey;
        $this->baseURL = $baseURL;
        $this->guzzleClientImpl = new GuzzleClient($this->baseURL, [
            'Api-Key' => $this->apiKey,
            'User-Agent' => 'curl/7.65.3'
        ]);
    }


    public function execute($paymentIntent): string
    {
        try {
            $data = $this->guzzleClientImpl->getClient()->get($this->getExecuteUri($paymentIntent['id']));
            return json_decode($data->getBody()->getContents(), true);
        } catch (GuzzleException $e) {

            throw new \Exception($e->getMessage());
        }
    }

    /**
     * @throws \Exception
     */
    public function createTransaction(TransactionData $createTransactionDTO)
    {
        $options['json'] = [
            'amount' => $createTransactionDTO->amount,
            'currency' => $createTransactionDTO->currency,
            'serverToServer' => $createTransactionDTO->serverToServer,
            'customer' => [
                'id' => $createTransactionDTO->customer->clientId,
                'email' => $createTransactionDTO->customer->email,
            ],
            'captureMethod' => $createTransactionDTO->captureMethod,
            'type' => $createTransactionDTO->type,
            'metadata' => $createTransactionDTO->metadata,
        ];
        $options['json'] += [
            'enabledProcessors' => $createTransactionDTO->paymentParams->enabledProcessors,
            'disabledProcessors' => $createTransactionDTO->paymentParams->disabledProcessors,
            'enabledMethods' => $createTransactionDTO->paymentParams->enabledMethods,
            'disabledMethods' => $createTransactionDTO->paymentParams->disabledMethods,
            'paymentCards' => $createTransactionDTO->paymentParams->paymentCards,
        ];
        $options['json'] += [
            'successUrl' => $createTransactionDTO->urlConfig->successUrl,
            'failedUrl' => $createTransactionDTO->urlConfig->failedUrl,
            'webhookUrl' => $createTransactionDTO->urlConfig->webhookUrl,
        ];

        return $this->sendRequest($options);
    }


    /**
     * @throws \Exception
     */
    private function sendRequest(array $options): PaymentIntent
    {
        try {
            $response = $this->guzzleClientImpl->getClient()->post('/api/client/payment/create', $options);
            $result = json_decode($response->getBody()->getContents(), true);

            return $this->getSerializer()->deserialize($result, PaymentIntent::class, 'json');
        } catch (GuzzleException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getExecuteUrl(string $id): string
    {
        return $this->baseURL . $this->getExecuteUri($id);
    }

    private function getExecuteUri(string $id): string
    {
        return '/api/client/payment/' . $id . '/execute';
    }

}
