<?php

namespace MountPay\Utils;

use GuzzleHttp\Client;

class GuzzleClient
{
    private array $defaultHeaders = ['Content-Type' => 'application/json'];
    private string $baseURL;
    private Client $client;

    public function __construct($baseURL, $defaultHeaders)
    {
        $this->baseURL = $baseURL;
        $this->defaultHeaders = array_merge($this->defaultHeaders, $defaultHeaders);
        $this->createNewGuzzleClient();
    }

    private function createNewGuzzleClient(): void
    {
        $this->client = new Client([
            'base_uri' => $this->baseURL,
            'timeout' => 30.0,
            'verify' => false,
            'headers' => $this->defaultHeaders
        ]);
    }

    public function getClient(): Client
    {
        return $this->client;
    }

    public function addDefaultHeaders(array $headers)
    {
        $this->defaultHeaders = array_merge($this->defaultHeaders, $headers);
        $this->createNewGuzzleClient();
    }

}
