<?php

namespace MountPay\Utils;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

trait SerializerTrait
{
    private ?Serializer $serializer = null;

    protected function getSerializer(): Serializer
    {
        if ($this->serializer === null) {
            $encoders = [new JsonEncoder()];
            $normalizers = [new ObjectNormalizer()];

            $this->serializer = new Serializer($normalizers, $encoders);
        }

        return $this->serializer;
    }

}